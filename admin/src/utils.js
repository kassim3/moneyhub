const axios = require("axios");
const config = require("config");

const getCompanies = async () => {
  try {
    const { data } = await axios.get(
      `${config.financialCompaniesServiceUrl}/companies`
    );

    return [null, data];
  } catch (e) {
    console.error(e);
    return [e, null];
  }
};

const getInvestments = async () => {
  try {
    const { data } = await axios.get(
      `${config.investmentsServiceUrl}/investments`
    );
    return [null, data];
  } catch (e) {
    console.error(e);
    return [e, null];
  }
};

const assignCompanyToInvestments = (investments, companies) => {
  return investments.map((investment) => {
    const company = companies.find(
      (company) => company.id === investment.userId
    );
    investment.company = company;
    return investment;
  });
};

const convertDataToCSV = (data) => {
  // const csv = [["User", "First Name", "Last Name", "Date", "Holding", "Value"]];
  let csv = "User,First Name,Last Name,Date,Holding,Value\n";

  for (let i = 0; i < data.length; i++) {
    const row = data[i];
    const values = row.holdings.map(
      (holding) => row.investmentTotal * holding.investmentPercentage
    );
    csv =
      csv +
      `"${row.userId}","${row.firstName}","${row.lastName}","${row.date}","${
        row.company.name
      }","${values.join("-")}"\n`;
  }

  return csv;
};

const sendCSVToInvestService = async (csv) => {
  console.log(csv);
  try {
    await axios.post(`${config.investmentsServiceUrl}/investments/export`, {
      csv,
    });
    return [null, null];
  } catch (e) {
    console.error(e);
    return [e, null];
  }
};

module.exports = {
  convertDataToCSV,
  getCompanies,
  getInvestments,
  assignCompanyToInvestments,
  sendCSVToInvestService,
};
