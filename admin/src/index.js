const express = require("express");
const bodyParser = require("body-parser");
const config = require("config");
const axios = require("axios");

const {
  getCompanies,
  getInvestments,
  assignCompanyToInvestments,
  convertDataToCSV,
  sendCSVToInvestService,
} = require("./utils");

const app = express();

app.use(bodyParser.json({ limit: "10mb" }));

app.get("/investments/:id", async (req, res) => {
  const { id } = req.params;

  try {
    const { data } = await axios.get(
      `${config.investmentsServiceUrl}/investments/${id}`
    );

    res.send(data);
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  }
});

app.get("/all-user-holdings", async (req, res) => {
  const [companyError, companies] = await getCompanies();
  if (companyError) {
    return res.sendStatus(500).send("Issue retrieving companies");
  }
  const [investmentError, investments] = await getInvestments();
  if (investmentError) {
    return res.sendStatus(500).send("Issue retrieving investments");
  }

  const data = assignCompanyToInvestments(investments, companies);
  const csv = convertDataToCSV(data);

  sendCSVToInvestService(csv);

  res.setHeader("content-type", "text/csv");
  res.send(csv);
});

app.listen(config.port, (err) => {
  if (err) {
    console.error("Error occurred starting the server", err);
    process.exit(1);
  }
  console.log(`Server running on port ${config.port}`);
});
